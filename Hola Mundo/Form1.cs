﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hola_Mundo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = ("Hola Mundo PCP 1-3");
            pictureBox1.Image = Properties.Resources.hi_hello;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = ("Adiós Mundo PCP 1-3");
            pictureBox1.Image = Properties.Resources._50db8ffc922f75940e0bda42b2b889f3cd8990ecr1_370_300_hq;
        }
    }
}
